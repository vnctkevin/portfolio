import Link from "next/link";
import Logo from "components/Logo";
import React, { useState } from "react";
import { Box, Flex, HStack, IconButton, useDisclosure, VStack, Text, useColorModeValue, Spacer } from "@chakra-ui/react";
import { CloseIcon, HamburgerIcon } from "@chakra-ui/icons";
import Toggle from "components/Toggle";
import Image from 'next/image'

const MENU_LIST = [
    { text: "Contact Me", href: "mailto:vnctkevin@gmail.com" },
    { text: "Blog", href: "" },
    { text: "Resume", href: "/assets/CV_Kevin.pdf" },
];
const Navbar = () => {
  const [navActive, setNavActive] = useState(null);
  const [activeIdx, setActiveIdx] = useState(-1);
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <Box
        as="nav"
        w="100%"
        bg={[ "transparent", "transparent"]}
        zIndex={1}
        borderBottomWidth={1}
        borderColor={useColorModeValue("gray.200", "gray.900")}
        color={useColorModeValue("gray.600", "gray.200")}
        >
        <Flex
            align="center"
            justify="space-between"
            wrap="wrap"
            py={2}
            px={4}
            bg={useColorModeValue("white", "gray.800")}
            borderBottomWidth={1}
            borderColor={useColorModeValue("gray.200", "gray.900")}
            color={useColorModeValue("gray.600", "gray.200")}
        >
            <Flex
                align="center"
                justify="space-between"
                wrap="wrap"
                gap={2}
                >

                <Image alt="Kevin" src={useColorModeValue("/logo-kevin-fit.png", "/logo-kevin-fit-white.png")} width={80} height={40}/>
            </Flex>
            <Box
                display={{ base: isOpen ? "block" : "none", md: "block" }}
                flexBasis={{ base: "100%", md: "auto" }}
            >
                <HStack
                    spacing={{ base: 4, md: 8 }}
                    align="center"
                    justify={["center", "space-between", "flex-end", "flex-end"]}
                    pt={[4, 4, 0, 0]}
                >
                    {MENU_LIST.map((item, idx) => (
                        <Box key={idx}>
                            <Link href={item.href}>
                                <Text color={useColorModeValue('gray.800', 'white')}>
                                    {item.text}
                                </Text>
                            </Link>
                        </Box>
                    ))}
                    <Toggle />
                </HStack>
            </Box>
            <IconButton
                icon={isOpen ? <CloseIcon /> : <HamburgerIcon />}
                display={{ base: "block", md: "none" }}
                onClick={isOpen ? onClose : onOpen}
                variant="ghost"
                aria-label="Toggle navigation"
            />
        </Flex>
    </Box>
  );
};

export default Navbar;
