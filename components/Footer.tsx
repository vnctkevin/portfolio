import { ReactNode } from 'react';
import {
  Box,
  Container,
  Stack,
  SimpleGrid,
  Text,
  Link,
  VisuallyHidden,
  chakra,
  useColorModeValue,
} from '@chakra-ui/react';
import { FaTwitter, FaYoutube, FaInstagram, FaGithub, FaMedium, FaGitlab, FaLinkedin } from 'react-icons/fa';
import {BsInstagram, BsTwitter, BsGithub, BsMedium, BsYoutube, BsGlobe} from 'react-icons/bs';
import { GrMail } from 'react-icons/gr';


const ListHeader = ({ children }: { children: ReactNode }) => {
  return (
    <Text fontWeight={'500'} fontSize={'lg'} mb={2}>
      {children}
    </Text>
  );
};

const SocialButton = ({
  children,
  label,
  href,
}: {
  children: ReactNode;
  label: string;
  href: string;
}) => {
  return (
    <chakra.button
      bg={useColorModeValue('blackAlpha.100', 'whiteAlpha.100')}
      rounded={'full'}
      w={8}
      h={8}
      cursor={'pointer'}
      as={'a'}
      href={href}
      display={'inline-flex'}
      alignItems={'center'}
      justifyContent={'center'}
      transition={'background 0.3s ease'}
      _hover={{
        bg: useColorModeValue('blackAlpha.200', 'whiteAlpha.200'),
      }}>
      <VisuallyHidden>{label}</VisuallyHidden>
      {children}
    </chakra.button>
  );
};

export default function Footer() {
  return (
    <Box
      bg={useColorModeValue('gray.50', 'gray.900')}
      color={useColorModeValue('gray.700', 'gray.200')}>
      

      <Box
        borderTopWidth={1}
        borderStyle={'solid'}
        borderColor={useColorModeValue('gray.200', 'gray.700')}>
        <Container
          as={Stack}
          maxW={'6xl'}
          py={8}
          direction={{ base: 'column', md: 'row' }}
          spacing={4}
          justify={{ md: 'space-between' }}
          align={ 'center' }>
          <Text>© 2024 Kevin. All rights reserved</Text>
          <Stack direction={'row'} spacing={6}>
            <SocialButton label={'Mail'} href={'mailto:vnctkevin@gmail.com'}>
                <GrMail />
            </SocialButton>
            <SocialButton label={'LinkedIn'} href={'https://www.linkedin.com/in/vnctkevin/'}>
              <FaLinkedin />
            </SocialButton>
            <SocialButton label={'Twitter'} href={'https://www.twitter.com/vnctkevin/'}>
              <FaTwitter />
            </SocialButton>
            <SocialButton label={'YouTube'} href={'https://www.youtube.com/@vnctkevin/videos'}>
              <FaYoutube />
            </SocialButton>
            <SocialButton label={'Instagram'} href={'https://www.instagram.com/vnctkevin/'}>
              <FaInstagram />
            </SocialButton>
            <SocialButton label={'Github'} href={'https://github.com/vnctkevin'}>
              <FaGithub />
            </SocialButton>
            <SocialButton label={'Gitlab'} href={'https://gitlab.com/vnctkevin'}>
              <FaGitlab />
            </SocialButton>
            <SocialButton label={'Medium'} href={'https://medium.com/@vnctkevin'}>
              <FaMedium />
            </SocialButton>
          </Stack>
        </Container>
      </Box>
    </Box>
  );
}