import Head from 'next/head'
import Image from 'next/image'
import { Inter } from '@next/font/google'
import * as React from 'react'
import Footer from 'components/Footer'
import Link from 'next/link'
// 1. import `ChakraProvider` component
import { ChakraProvider } from '@chakra-ui/react'

import {
  Box,
  Heading,
  Container,
  Text,
  Button, ButtonGroup,
  Stack,
  Icon,
  useColorModeValue,
  createIcon,
  Center,
  Card, CardHeader, CardBody, CardFooter,
  Tabs, TabList, TabPanels, Tab, TabPanel,
} from '@chakra-ui/react';

const inter = Inter({ subsets: ['latin'] })

export default function Hero() {
  return (
    <ChakraProvider>
    <>
      <Container maxW={"3xl"}>
        <Stack
          as={Box}
          textAlign={'center'}
          justify={'center'}
          alignContent={'center'}
          spacing={{ base: 8, md: 16 }}
          pt={{ base: 20, md: 36 }}
          pb={{ base: 8, md: 16}}>
          <Heading
            fontWeight={800}
            fontSize={{ base: '2xl', sm: '4xl', md: '6xl' }}
            lineHeight={'110%'}>
            Hello! My name is <br /> 
            <Text as={'span'} color={'blue.500'}>Kevin</Text></Heading>
            <Text as={'span'}>
            I synthesize ideas into visuals and code.
          </Text>
          <Text color={useColorModeValue('gray.700', 'gray.200')}>
          <b>I am a student, a developer, and a designer.</b> I am a creative and dedicated person who's passionate about 
                <b> visual design, UI/UX design, and software and web development. </b>
                Currently, I am studying Computer Science at University of Indonesia as an undergraduate student. 
                After four years of experience in the creative field, I have helped 
                people visualize their dreams and visions, and I will be more than happy 
                to cooperate with you.
          </Text>
          <Stack
            direction={{ base: 'column', md: 'row' }}
            spacing={3}
            align={'center'}
            alignSelf={'center'}
            position={'relative'}>
              <Link href="/assets/CV_Kevin.pdf">
                <Button
                  bg={useColorModeValue('black', 'gray.100')}
                  px={6}
                  _hover={{
                    bg: 'gray-600',
                  }}>
                  <Text color={useColorModeValue('white', 'gray.700')}>Download CV</Text>
                </Button>
              </Link>
              <Link href="/assets/Portofolio Documentation.pdf">
                <Button
                  colorScheme={'black'}
                  border={'1px solid black'}
                  bg={useColorModeValue('white', 'gray.900')}
                  px={6}
                  _hover={{
                    bg: 'gray-600',
                  }}>
                  <Text color={useColorModeValue('gray.700', 'gray.200')}>Download Portfolio</Text>
                </Button>
            </Link>
          </Stack>
          </Stack>
          </Container>
    </>
    </ChakraProvider>
  )
} 

