import { ChakraProvider } from '@chakra-ui/react'
import {
    Box,
    Heading,
    Container,
    Text,
    Button, ButtonGroup,
    Stack,
    Icon,
    useColorModeValue,
    createIcon,
    Center,
    Card, CardHeader, CardBody, CardFooter,
    Tabs, TabList, TabPanels, Tab, TabPanel,
    Spacer,
    UnorderedList, ListItem,
  } from '@chakra-ui/react';

  import {motion} from 'framer-motion'

export default function Knowledge() {
    return (
        <>
        <Container maxW={"5xl"}>
                <Stack
                as={Box}
                textAlign={'center'}
                spacing={{ base: 8, md: 12 }}
                py={{ base: 8, md: 16 }}>
                <Heading
                    fontWeight={800}
                    fontSize={{ base: '2xl', sm: '4xl', md: '6xl' }}
                    lineHeight={'110%'}>
                    Skills & Knowledge <br /> </Heading>
                    <Text as={'span'}>
                        All of the skills and fields I've professionally worked on so far.
                    </Text>
                </Stack>
                <motion.div
                        initial={{ opacity: 0, y: 100 }}
                        whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5 }}}
                        >
                <Stack
                    direction={{ base: 'column', md: 'row' }}
                    spacing={{ base: 10, md: 4 }}
                    justify={{ md: 'space-between' }}
                    mb={8}>
                    
                    <Box
                        rounded={'lg'}
                        bg={useColorModeValue('white', 'gray.700')}
                        boxShadow={'lg'}
                        p={8}>
                        <Stack spacing={4}>
                        <Heading
                            color={useColorModeValue('gray.800', 'white')}
                            fontSize={'xl'}>
                            Software Engineering
                        </Heading>
                        <Text
                            color={useColorModeValue('gray.500', 'gray.400')}
                            fontSize={'md'}>
                            Worked on several projects as a Software Engineer, ranging from web development to mobile development.
                        </Text>
                        </Stack>
                    </Box>
                    <Box
                        rounded={'lg'}
                        bg={useColorModeValue('white', 'gray.700')}
                        boxShadow={'lg'}
                        p={8}>
                        <Stack spacing={4}>
                        <Heading
                            color={useColorModeValue('gray.800', 'white')}
                            fontSize={'xl'}>
                            Visual Design
                        </Heading>
                        <Text
                            color={useColorModeValue('gray.500', 'gray.400')}
                            fontSize={'md'}>
                            Helped and been helped by people to realize their visions through graphic design for more than five years.
                        </Text>
                        </Stack>
                    </Box>
                    <Box
                        rounded={'lg'}
                        bg={useColorModeValue('white', 'gray.700')}
                        boxShadow={'lg'}
                        p={8}>
                        <Stack spacing={4}>
                        <Heading
                            color={useColorModeValue('gray.800', 'white')}
                            fontSize={'xl'}>
                            UI/UX Design
                        </Heading>
                        <Text
                            color={useColorModeValue('gray.500', 'gray.400')}
                            fontSize={'md'}>
                            Worked on several organizational and professional projects to build UI/UX design solutions.
                        </Text>
                        </Stack>
                    </Box>
                    </Stack>
                    </motion.div>
                </Container>
            </>
    )
}