import Head from 'next/head'
import Image from 'next/image'
import { Inter } from '@next/font/google'
import * as React from 'react'
import Footer from 'components/Footer'
import Experiences from 'components/Experiences'
import Hero from 'components/Hero'
import Knowledge from 'components/Knowledge'
import BlogCTA from 'components/BlogCTA'
import Navbar from 'components/Navbar'

// 1. import `ChakraProvider` component
import { ChakraProvider } from '@chakra-ui/react'

import {
  Box,
  Heading,
  Container,
  Text,
  Button, ButtonGroup,
  Stack,
  Icon,
  useColorModeValue,
  createIcon,
  Center,
  Card, CardHeader, CardBody, CardFooter,
  Tabs, TabList, TabPanels, Tab, TabPanel,
} from '@chakra-ui/react';


const inter = Inter({ subsets: ['latin'] })
import theme from '../theme' 

export default function Home() {
  return (
    <>
    <ChakraProvider theme={theme}>
    
      <Head>
        <title>Kevin</title>
        <meta name="description" content="Kevin (@vnctkevin) portfolio and blog website" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon-32x32.png" />
      </Head>
          <Navbar/>
          <Hero />
          <div id="experience">
            <Experiences/>
          </div>
          <Knowledge />  
          <BlogCTA />
      <Footer/>
    
    </ChakraProvider>
    </>
  )
} //change font to inter

