import { ChakraProvider } from '@chakra-ui/react'
import {
    Box,
    Heading,
    Container,
    Text,
    Button, ButtonGroup,
    Stack,
    Icon,
    useColorModeValue,
    createIcon,
    Center,
    Card, CardHeader, CardBody, CardFooter,
    Tabs, TabList, TabPanels, Tab, TabPanel,
    Spacer,
    UnorderedList, ListItem,
  } from '@chakra-ui/react';
import { useRouter } from 'next/router'


export default function BlogCTA() {
    const router = useRouter()
    const handleClick = () => {
        router.push("https://jurnal.vnctkevin.com/")
    }
    return (
        <>
        <Container maxW={"full"} bgColor={useColorModeValue("gray.900","gray.400")} textColor={"white"}>
                <Stack
                as={Box}
                textAlign={'center'}
                spacing={{ base: 8, md: 12 }}
                py={{ base: 12, md: 20 }}>
                <Heading
                    fontWeight={800}
                    fontSize={{ base: '2xl', sm: '4xl' }}
                    color={useColorModeValue("gray.200","gray.900")}
                    lineHeight={'110%'}>
                    Want to learn more? 
                    <Button textDecoration={"underline"} 
                    marginLeft={{base: 0, sm: 4}}
                    marginTop={{base: 8, sm: 0}}
                    fontSize={"xl"}
                    textColor={useColorModeValue("gray.200","gray.700")}
                    bg={"transparent"} 
                    _hover={{ bg: 'gray.600', color: 'white' }}
                    width={"fit-content"} 
                    alignSelf={"center"}
                    padding={"8"}
                    onClick={handleClick}>
                        Check out my blog!
                        </Button>
                        </Heading>
                </Stack>
                
                </Container>
            </>
    )
}