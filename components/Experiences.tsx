import Head from 'next/head'
import Image from 'next/image'
import { Inter } from '@next/font/google'
import * as React from 'react'
import Footer from 'components/Footer'
import { EXPERIENCES } from "constants/experiences"
import { ExperiencesType, SingularExperienceType } from "constants/experiences/types"
import { ChakraProvider } from '@chakra-ui/react'
import { useState } from 'react'
import {
  Box,
  Heading,
  Container,
  Text,
  Button, ButtonGroup,
  Stack,
  Icon,
  useColorModeValue,
  createIcon,
  Center,
  Card, CardHeader, CardBody, CardFooter,
  Tabs, TabList, TabPanels, Tab, TabPanel,
  Spacer,
  UnorderedList, ListItem,
} from '@chakra-ui/react';
import Link from 'next/link'
import {motion} from 'framer-motion'

const inter = Inter({ subsets: ['latin'] })


export default function Experiences() {
  //get all experiences
  const allExperiences = EXPERIENCES
  const experienceKeys: (keyof ExperiencesType)[] = Object.keys(
    EXPERIENCES
  ) as (keyof ExperiencesType)[];

  const [experienceTabs, setExperienceTabs] = useState<number>(0);

  //render singular experience
  const selectedExperience: SingularExperienceType[] =
      EXPERIENCES[experienceKeys[experienceTabs]];

  const renderExperiences = (experiences: SingularExperienceType[]) => {
      return experiences.map((experience: SingularExperienceType) => {
        return (
          <>
          <motion.div
          initial={{ opacity: 0, y: 100 }}
          whileInView={{ opacity: 1, y: 0, transition: { duration: 0.5 }}}
          viewport={{ once: true }}
          >
          <Card direction={{ base: 'column', md: 'row' }} my={4}>
            <CardBody>
              <Stack mt='2' spacing='2' align={"baseline"}>
                <Heading size='md' textAlign={"left"}>{experience.name}</Heading>
                <Text>
                  {experience.headline_role ?? experience.history?.[0]?.role ?? ""}
                </Text>
                </Stack>
                <Stack mt='2' spacing='2' align={"baseline"}>
                <UnorderedList textAlign={'left'}>
                  {experience.history[0].job_desc?.map((desc, i) => (
                    <ListItem>
                      {desc}
                    </ListItem>
                      ))}
                </UnorderedList>
              </Stack>
              <Stack spacing='4' mt='4' fontSize='sm' direction={"row"}>
                {experience.skills?.map((skill, i) => (
                  <Link href={skill.link} key={i}>
                    <Image src={skill.logo} alt={skill.name} width={24} height={24} />
                  </Link>
                )
                )}
                </Stack>
            </CardBody>
            <CardFooter>
              <Stack mt='1' spacing='3' maxW={"xl"} align={{base:"baseline" , md: "end"}}>
                <Text>
                  {experience.history[0].date}
                </Text>
                <Spacer/>
                <Link href={experience.link}>
                  <Button bg={useColorModeValue('black', 'gray.100')}
                    px={6}
                    _hover={{
                      bg: 'gray-600',
                    }}
                    textColor={useColorModeValue('white', 'gray.700')}>
                    More
                  </Button>
                </Link>
              </Stack>
            </CardFooter>
          </Card>
          </motion.div>
          </>
        )
      })
  }

  return (
    <ChakraProvider>
    <>
        <Container maxW={"3xl"}>
          <Stack
          as={Box}
          textAlign={'center'}
          spacing={{ base: 8, md: 12 }}
          py={{ base: 8, md: 16 }}>
          <Heading
            fontWeight={800}
            fontSize={{ base: '2xl', sm: '4xl', md: '6xl' }}
            lineHeight={'110%'}>
            Experiences <br /> </Heading>
            <Text as={'span'}>
            This includes (but not limited to, i suppose?) my works as a Software Engineer, UI/UX Designer, and Graphic Designer
          </Text>
          <Tabs isFitted>
          <TabList>
            <Tab>Works</Tab>
            <Tab>Projects</Tab>
            <Tab>Organizations</Tab>
          </TabList>
          <TabPanels>
            <TabPanel>
              {renderExperiences(allExperiences.Work)}
            </TabPanel>
            <TabPanel>
              {renderExperiences(allExperiences.Projects)}
            </TabPanel>
            <TabPanel>
              {renderExperiences(allExperiences.Organizations)}
            </TabPanel>
            </TabPanels>
        </Tabs>
          </Stack>
      </Container>

    </>
    </ChakraProvider>
  )
} //change font to inter

